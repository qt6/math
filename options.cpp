#include "options.h"

#include <QApplication>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

#include <QFile>

#include <QRandomGenerator64>

QString Paths::options = "config.json";

void Paths::init()
{

}

QString Paths::get(const QString &key)
{
    return qApp->applicationDirPath() + '/' + key;
}


OptionSaver::OptionSaver()
{
    Paths::init();

    QJsonObject json;
    if (Paths::get(Paths::options).contains(".json") && load(json, Paths::get(Paths::options)))
    {
        GeneralOptions::global().load(json["GeneralOptions"].toObject());
        PersonalOptions::global().load(json["PersonalConfigs"].toArray());
    }

}

OptionSaver::~OptionSaver()
{
    if (Paths::get(Paths::options).contains(".json"))
    {
        QJsonObject json;
        json["GeneralOptions"] = GeneralOptions::global().save();
        json["PersonalConfigs"] = PersonalOptions::global().save();

        save(json, Paths::get(Paths::options));
    }

}

bool OptionSaver::save(const QJsonObject &json, const QString &fileName)
{
    QFile saveFile(fileName);
    if (!saveFile.open(QIODevice::WriteOnly))
        return false;

    QJsonDocument document(json);
    saveFile.write(document.toJson());
    saveFile.close();

    return true;

}

bool OptionSaver::load(QJsonObject &json, const QString &fileName)
{
    QFile loadFile(fileName);
    if (!loadFile.open(QFile::ReadOnly))
        return false;

    QJsonDocument document(QJsonDocument::fromJson(loadFile.readAll()));
    json = document.object();
    loadFile.close();

    return true;

}

QJsonObject PersonalConfig::save() const
{
    QJsonObject json;

    json["name"] = name;
    json["minVal"] = minVal;
    json["maxVal"] = maxVal;
    json["count"] = count;
    json["countOperands"] = countOperands;
    json["repeat"] = repeat;
    json["mix"] = mix;
    json["samples"] = samples;
    json["inequlaity"] = inequlaity;

    json["isLimited"] = isLimited;
    json["isPlus"] = isPlus;
    json["isMinus"] = isMinus;
    json["isMult"] = isMult;
    json["isDev"] = isDev;
    json["isColumn"] = isColumn;
    json["bestTime"] = bestTime.toString("hh:mm:ss");
    json["limitTime"] = limitTime.toString("hh:mm:ss");

    return json;
}

void PersonalConfig::load(const QJsonObject &json)
{
    isPlus = json["isPlus"].toBool();
    isMult = json["isMult"].toBool();
    isDev = json["isDev"].toBool();
    isMinus = json["isMinus"].toBool();
    isColumn = json["isColumn"].toBool();
    bestTime = QTime::fromString(json["bestTime"].toString(), "hh:mm:ss");
    limitTime = QTime::fromString(json["limitTime"].toString(), "hh:mm:ss");

    isLimited = json["isLimited"].toBool();
    samples = json["samples"].toBool();
    inequlaity = json["inequlaity"].toBool();
    mix = json["mix"].toBool();
    repeat = json["repeat"].toBool();
    count = json["count"].toInt();
    maxVal = json["maxVal"].toInt();
    minVal = json["minVal"].toInt();
    countOperands = json["countOperands"].toInt();
    name = json["name"].toString();

}

PersonalOptions::PersonalOptions()
{
    reset();
}

void PersonalOptions::reset()
{
    configs.clear();
}

QJsonArray PersonalOptions::save() const
{
    QJsonArray json;

    for(const auto &config : configs){

        json.append(config.save());
    }

    return json;
}

void PersonalOptions::load(const QJsonArray &json)
{

    configs.clear();

    for(const auto &configObj : json){

        PersonalConfig tmpConf;

        tmpConf.load(configObj.toObject());

        configs.append(tmpConf);

    }
}



GeneralOptions::GeneralOptions()
{
    reset();
}

int GeneralOptions::randomVal(int minVal, int maxVal, bool includeMax)
{
    return QRandomGenerator64::global()->bounded(minVal, maxVal + includeMax);
}

int GeneralOptions::randomVal(int maxVal, bool includeMax)
{
    return randomVal(0, maxVal, includeMax);
}

void GeneralOptions::reset()
{

}

QJsonObject GeneralOptions::save() const
{
    QJsonObject json;

    json["lastConfig"] = lastConfig.save();

    return json;
}

void GeneralOptions::load(const QJsonObject &json)
{
    lastConfig.load(json["lastConfig"].toObject());
}


