#include "resultwidget.h"

#include <QTimeEdit>
#include <QTableWidget>
#include <QSpinBox>
#include <QHeaderView>
#include <QFormLayout>

#include "options.h"


CommonTab::CommonTab(QWidget *parent)
    :QWidget(parent)
{

    auto mainLay = new QFormLayout(this);

    m_resTable = new QTableWidget(0, 4, this);

    m_rightSB = new QSpinBox(this);
    m_wrongSB = new QSpinBox(this);

    m_calcTime = new QTimeEdit(this);


    m_countRight = 0;
    m_countWrong = 0;

    m_rightSB->setButtonSymbols(QSpinBox::NoButtons);
    m_wrongSB->setButtonSymbols(QSpinBox::NoButtons);
    m_calcTime->setButtonSymbols(QSpinBox::NoButtons);

    m_calcTime->setAlignment(Qt::AlignCenter);

    m_calcTime->setDisplayFormat("hh:mm:ss");

    m_rightSB->setReadOnly(true);
    m_wrongSB->setReadOnly(true);
    m_calcTime->setReadOnly(true);


    m_resTable->setSelectionMode(QTableWidget::SingleSelection);
    m_resTable->setSelectionBehavior(QTableWidget::SelectRows);
    m_resTable->setEditTriggers(QTableWidget::NoEditTriggers);
    m_resTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);


    mainLay->addRow(m_calcTime);
    mainLay->addRow(m_resTable);
    mainLay->addRow("Количество правильных ответов", m_rightSB);
    mainLay->addRow("Количество неправильных ответов", m_wrongSB);
}



SampleTab::SampleTab(QWidget *parent)
    :CommonTab(parent)
{
    m_resTable->setHorizontalHeaderLabels(QStringList()<<"Пример"<<"Правильный ответ"<<"Твой ответ"<<"Проверка ответа");
}

void SampleTab::updateResults()
{

    const auto &conf = GeneralOptions::global().lastConfig;

    m_rightSB->setMaximum(conf.resultsSamples.size());
    m_wrongSB->setMaximum(conf.resultsSamples.size());
    m_resTable->setRowCount(conf.resultsSamples.size());

    m_calcTime->setTime(conf.bestTime);

    for(int index = 0; index<conf.resultsSamples.size();++index){

        auto curRes = conf.resultsSamples.at(index);

        QString samleStr;

        for (int index = 0; index < curRes.signs.size(); ++index)
            samleStr += (QString::number(curRes.vals.at(index)) + QString(curRes.signs.at(index)));

        samleStr += QString::number(curRes.vals.last());

        m_resTable->setItem(index, 0, new QTableWidgetItem(samleStr));
        m_resTable->setItem(index, 1, new QTableWidgetItem(QString("%0").arg(curRes.resVal)));
        m_resTable->setItem(index, 2, new QTableWidgetItem(QString("%0").arg(curRes.ansVal)));

        auto tmpItem=new QTableWidgetItem;
        if(curRes.isRight){
            tmpItem->setText("Верно");
            tmpItem->setBackground(QColor(127,255,0));
            ++m_countRight;
        }else{
            tmpItem->setText("Не верно");
            tmpItem->setBackground(QColor(255,126,147));
            ++m_countWrong;
        }

        m_resTable->setItem(index, 3, tmpItem);

    }


    m_rightSB->setValue(m_countRight);
    m_wrongSB->setValue(m_countWrong);

}

InequalityTab::InequalityTab(QWidget *parent)
    :CommonTab(parent)
{

    m_resTable->setHorizontalHeaderLabels(QStringList()<<"Неравенство"<<"Правильный ответ"<<"Твой ответ"<<"Проверка ответа");

}

void InequalityTab::updateResults()
{
    auto conf = GeneralOptions::global().lastConfig;

    m_rightSB->setMaximum(conf.resultsInequality.size());
    m_wrongSB->setMaximum(conf.resultsInequality.size());

    m_calcTime->setTime(conf.bestTime);

    m_resTable->setRowCount(conf.resultsInequality.size());

    for(int index = 0; index<conf.resultsInequality.size();++index){

        auto curRes = conf.resultsInequality.at(index);

        auto lEx = curRes.leftExample;
        auto rEx = curRes.rightExample;

        //        resTable->setItem(index, 0, new QTableWidgetItem(QString("%0 %1 %2 _ %3 %4 %5").arg(lEx.fVal).arg(lEx.sign).arg(lEx.sVal).arg(rEx.fVal).arg(rEx.sign).arg(rEx.sVal)));
        m_resTable->setItem(index, 1, new QTableWidgetItem(QString("%0").arg(curRes.resSign)));
        m_resTable->setItem(index, 2, new QTableWidgetItem(QString("%0").arg(curRes.ansSign)));

        auto tmpItem=new QTableWidgetItem;
        if(curRes.isRight){
            tmpItem->setText("Верно");
            tmpItem->setBackground(QColor(127,255,0));
            ++m_countRight;
        }else{
            tmpItem->setText("Не верно");
            tmpItem->setBackground(QColor(255,126,147));
            ++m_countWrong;
        }

        m_resTable->setItem(index, 3, tmpItem);


    }


    m_rightSB->setValue(m_countRight);
    m_wrongSB->setValue(m_countWrong);
}

ResultWidget::ResultWidget(QWidget *parent)
    : QTabWidget{parent}
{
    addTab(m_sampleTab = new SampleTab(this), "Примеры");
    addTab(m_inequalityTab = new InequalityTab(this), "Неравенства");
}

void ResultWidget::updateResults()
{
    m_sampleTab->updateResults();
    m_inequalityTab->updateResults();
}


