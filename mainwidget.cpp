#include "mainwidget.h"

#include <QRandomGenerator64>
#include <QTime>
#include <QLCDNumber>
#include <QLabel>
#include <QLayout>

#include <QKeyEvent>
#include <cmath>

#include "dragdroplabel.h"
#include "options.h"
#include "info.h"

CommonWidget::CommonWidget(QWidget *parent)
    :QWidget(parent)
{
    const auto &conf = GeneralOptions::global().lastConfig;

    setAttribute(Qt::WA_DeleteOnClose);
    setFocusPolicy(Qt::ClickFocus);

    m_countForExit = 0;

    len = calc();

    sampleLay = new QHBoxLayout(this);

    m_limitTimer = new QTimer(this);

    m_limitTimer->setSingleShot(true);
    m_limitTimer->callOnTimeout(this, &CommonWidget::checkAnswer);


    // configLabel(m_lostLbl, 20);


    m_operandsLCDs.clear();
    m_operatorsLbl.clear();


    for(int index = 0; index < conf.countOperands; ++index){

        auto tmpLCD = new QLCDNumber(len, this);

        m_operandsLCDs.append(tmpLCD);

        sampleLay->addWidget(tmpLCD);
    }

    for(int index = 0; index < conf.countOperands - 1; ++index){

        auto tmpLbl = new QLabel(this);

        m_operatorsLbl.append(tmpLbl);
        configLabel(tmpLbl, 36);

        sampleLay->insertWidget(2 * index + 1, tmpLbl);
    }


}

void CommonWidget::generate()
{
    if(m_limitTimer->isActive())
        m_limitTimer->stop();
}

void CommonWidget::configLabel(QLabel *lbl ,int size)
{
    lbl->setAlignment(Qt::AlignCenter);
    QFont tmpFont = lbl->font();
    tmpFont.setPointSize(size);
    tmpFont.setBold(true);
    lbl->setFont(tmpFont);

}

int CommonWidget::calc() const
{
    int digitCount = 0;

    const auto cfg = GeneralOptions::global().lastConfig;

    int num = (cfg.signs.contains('*') || cfg.signs.contains(':')) ? qMax(cfg.maxVal, 81) : cfg.maxVal;

    while(num){
        ++digitCount;
        num /= 10;
    }

    return digitCount;

}

void CommonWidget::keyPressEvent(QKeyEvent *e)
{
    if(e->key() == Qt::Key_Enter || e->key()==Qt::Key_Return )
        checkAnswer();

    QWidget::keyPressEvent(e);
}

void CommonWidget::checkAnswer()
{

    const auto conf = GeneralOptions::global().lastConfig;


    if(conf.lost){
        emit answered();
    }else {
        m_limitTimer->stop();
        emit finished();
    }

}

int CommonWidget::generateValues(int &fVal, int &sVal, char sign, bool canChangeFirst) const
{

    const auto conf = GeneralOptions::global().lastConfig;

    int out = 0;

    int aNum = fVal;
    int bNum = sVal;
    int cNum = out;

    bool isRight = false;

    switch (sign) {
    case '+':
    case '-':
        do{

            if(canChangeFirst){
                aNum = GeneralOptions::randomVal(conf.minVal, conf.maxVal);
            }

            bNum = GeneralOptions::randomVal(conf.minVal, conf.maxVal);

            cNum = aNum + bNum;

            if(sign == '+'){
                if(canChangeFirst)
                    fVal = aNum;
                sVal = bNum;
                out = cNum;

                //                isRight = out < conf.maxVal;
            }
            else {
                if(canChangeFirst)
                    fVal = cNum;
                sVal = bNum;
                out = fVal - sVal;

                //                isRight = out < conf.maxVal;
            }

        }while(qMax(out, cNum) > conf.maxVal || qMax(out, cNum) < conf.minVal);
        break;
    case '*':
    case ':':
        do{
            if(canChangeFirst)
                aNum =  GeneralOptions::randomVal(1, 9);

            bNum =  GeneralOptions::randomVal(1, 9);

            cNum = aNum * bNum;

            if(sign == '*'){


                if(canChangeFirst)
                    fVal = aNum;

                sVal = bNum;
                out = cNum;

                isRight = (out >= 1 && out <= 81);

            }
            else{

                if(canChangeFirst)
                    fVal = cNum;

                sVal = bNum;
                out = fVal / sVal;

                isRight = ! (fVal % sVal) && ((out >= 1 ) && (out <= 9));
            }
        }while(!isRight);
        break;
    }


    return out;
}

void CommonWidget::generateSample(SampleInfo &sample)
{

    const auto &conf = GeneralOptions::global().lastConfig;

    int fNum = 0;
    int sNum = 0;

    int out = 0;

    do{

        sample.vals.clear();
        sample.signs.clear();

        auto copySigns = conf.signs;

        for(int i = 0; i < conf.countOperands - 1; ++i){

            auto randSign = copySigns.at(GeneralOptions::randomVal(copySigns.size(), false));


            fNum = out;
            sNum = 0;

            out = generateValues(fNum, sNum, randSign, !i);

            if(!i){
                sample.vals.append(fNum);
            }

            sample.vals.append(sNum);

            sample.signs.append(randSign);

        }

    }while(out > conf.maxVal || out < conf.minVal);

    sample.resVal = out;


}

int CommonWidget::tranlateTimeToSecs(const QTime &time) const
{
    return time.hour() * 3600 + time.minute() * 60 + time.second();
}

void MainWidget::updateLost()
{
    auto conf = GeneralOptions::global().lastConfig;

    m_lostLbl->setText(QString("Осталось: %0").arg(conf.lost));
}


SampleWidget::SampleWidget(QWidget *parent)
    : CommonWidget{parent}
{

    auto mainLay = new QVBoxLayout(this);




    m_ansVal = new QLCDNumber(len, this);



    m_enterVal = nullptr;

    auto equalLbl = new QLabel("=", this);


    m_countForExit = 0;

    configLabel(equalLbl);



    sampleLay->addWidget(equalLbl);
    sampleLay->addWidget(m_ansVal);

    mainLay->addLayout(sampleLay, 1);
    // mainLay->addWidget(m_lostLbl);


    setLayout(mainLay);

}

void SampleWidget::generate()
{

    CommonWidget::generate();

    const auto &curConf = GeneralOptions::global().lastConfig;
    auto &info=SampleExtInfo::global();


    generateSample(info);


    for(int index = 0; index < info.vals.size(); ++index){
        m_operandsLCDs.at(index)->display(info.vals.at(index));
    }


    for(int index = 0; index < info.signs.size(); ++index){
        m_operatorsLbl.at(index)->setText(QString(info.signs.at(index)));
    }


    m_enterVal = m_ansVal;

    m_enterVal->display(0);
    m_enterVal->setSegmentStyle(QLCDNumber::Outline);


    if(curConf.isLimited)
        m_limitTimer->start(tranlateTimeToSecs(curConf.limitTime) * 1000);

}

void SampleWidget::keyPressEvent(QKeyEvent *e)
{
    if(e->key()==Qt::Key_Backspace){
        m_enterVal->display(m_enterVal->intValue() / 10);
    }else if(e->key() >= Qt::Key_0 && e->key() <= Qt::Key_9){

        int curVal = m_enterVal->intValue();
        int nexDig = e->text().toInt();
        m_enterVal->display((10 * curVal + nexDig) % static_cast<int>(pow(10, m_enterVal->digitCount())));

    }

    if(m_enterVal->intValue()){
        m_enterVal->setSegmentStyle(QLCDNumber::Filled);
    }else{
        m_enterVal->setSegmentStyle(QLCDNumber::Outline);
    }

    CommonWidget::keyPressEvent(e);
}

void SampleWidget::checkAnswer()
{
    auto &conf = GeneralOptions::global().lastConfig;
    auto &sampleInfo= SampleExtInfo::global();


    sampleInfo.ansVal = m_ansVal->intValue();
    sampleInfo.isRight = (sampleInfo.ansVal == sampleInfo.resVal);

    conf.lost -= conf.repeat ? sampleInfo.isRight : 1;

    conf.resultsSamples.append(sampleInfo);

    if(conf.repeat && !sampleInfo.isRight)
        m_enterVal->display(0);
    else
        CommonWidget::checkAnswer();

}

char SampleWidget::invertSign(char sign)
{
    switch (sign) {
    case '+':
        return '-';
    case '-':
        return '+';
    case '*':
        return ':';
    case ':':
        return '*';
    default:
        return '\0';
    }
}

InequalityWidget::InequalityWidget(QWidget *parent)
    :CommonWidget(parent)
{

    auto mainLay = new QVBoxLayout(this);
    auto sampleLay = new QHBoxLayout(this);
    auto signLay = new QHBoxLayout(this);

    auto lessSign = new DragLabel("<", this);
    auto eqSign = new DragLabel("=", this);
    auto moreSign = new DragLabel(">", this);



    int len = calc();


    m_leftFirstVal = new QLCDNumber(len, this);
    m_leftSignLbl = new QLabel(this);
    m_leftSecondVal = new QLCDNumber(len, this);

    m_compareSignLbl = new DropLabel("_", this);

    m_rightFirstVal = new QLCDNumber(len, this);
    m_rightSignLbl = new QLabel(this);
    m_rightSecondVal = new QLCDNumber(len, this);




    configLabel(m_leftSignLbl);
    configLabel(m_rightSignLbl);
    configLabel(m_compareSignLbl);
    configLabel(lessSign, 30);
    configLabel(eqSign, 30);
    configLabel(moreSign, 30);
    // configLabel(m_lostLbl, 10);

    sampleLay->addWidget(m_leftFirstVal);
    sampleLay->addWidget(m_leftSignLbl);
    sampleLay->addWidget(m_leftSecondVal);

    sampleLay->addWidget(m_compareSignLbl);

    sampleLay->addWidget(m_rightFirstVal);
    sampleLay->addWidget(m_rightSignLbl);
    sampleLay->addWidget(m_rightSecondVal);


    signLay->addWidget(lessSign);
    signLay->addWidget(eqSign);
    signLay->addWidget(moreSign);

    mainLay->addLayout(signLay);
    mainLay->addLayout(sampleLay);
    // mainLay->addWidget(m_lostLbl);

}

void InequalityWidget::generate()
{

    CommonWidget::generate();


    auto conf = GeneralOptions::global().lastConfig;

    auto &inEqInfo = InequalityInfo::global();


    generateSample(inEqInfo.leftExample);
    generateSample(inEqInfo.rightExample);



    //    for(int index = 0; index < 5; ++index){
    //        m_curLeftSign = conf.signs.at(GeneralOptions::randomVal(conf.signs.size(), false));
    //    }
    //    for(int index = 0; index < 5; ++index){
    //        m_curRightSign = conf.signs.at(GeneralOptions::randomVal(conf.signs.size(), false));
    //    }


    //    int fNum = 0;
    //    int sNum = 0;
    //    int rNum = 0;

    //    rNum = generateValues(fNum, sNum, m_curLeftSign);

    //    switch (m_curLeftSign) {
    //    case '+':
    //    case '*':
    //        m_leftFirstVal->display(fNum);
    //        m_leftSecondVal->display(sNum);
    //        break;
    //    case '-':
    //    case ':':
    //        m_leftFirstVal->display(rNum);
    //        m_leftSecondVal->display(sNum);
    //    default:
    //        break;
    //    }

    //    rNum = generateValues(fNum, sNum, m_curRightSign);

    //    switch (m_curRightSign) {
    //    case '+':
    //    case '*':
    //        m_rightFirstVal->display(fNum);
    //        m_rightSecondVal->display(sNum);
    //        break;
    //    case '-':
    //    case ':':
    //        m_rightFirstVal->display(rNum);
    //        m_rightSecondVal->display(sNum);
    //    default:
    //        break;
    //    }



    //    m_compareSignLbl->setText("_");
    //    m_leftSignLbl->setText(QString(m_curLeftSign));
    //    m_rightSignLbl->setText(QString(m_curRightSign));
}

void InequalityWidget::keyPressEvent(QKeyEvent *e)
{
    CommonWidget::keyPressEvent(e);
}

void InequalityWidget::checkAnswer()
{

    /*
    auto &conf = GeneralOptions::global().lastConfig;


    InequalityInfo res;

    auto &lEx = res.leftExample;
    auto &rEx = res.rightExample;

    lEx.fVal = m_leftFirstVal->intValue();
    lEx.sign = m_curLeftSign;
    lEx.sVal = m_leftSecondVal->intValue();
    calResVal(lEx);

    rEx.fVal = m_rightFirstVal->intValue();
    rEx.sign = m_curRightSign;
    rEx.sVal = m_rightSecondVal->intValue();
    calResVal(rEx);

    if(lEx.resVal > rEx.resVal)
        res.resSign = '>';
    else if(lEx.resVal < rEx.resVal)
        res.resSign = '<';
    else
        res.resSign = '=';


    res.ansSign = m_compareSignLbl->text().toLatin1().at(0);

    res.isRight = (res.resSign == res.ansSign);

    conf.lost -= conf.repeat ? res.isRight : 1;

    conf.resultsInequality.append(res);

    if(conf.repeat && !res.isRight)
        m_compareSignLbl->setText("_");
    else
        CommonWidget::checkAnswer();

    */
}

MainWidget::MainWidget(QWidget *parent)
    : QWidget(parent)
{


    auto mainLay = new QVBoxLayout(this);

    m_upperW = new QStackedWidget(this);

    m_lostLbl = new QLabel(this);

    m_sampleWidget = nullptr;
    m_inequalityWidget = nullptr;


    mainLay->addWidget(m_upperW);
    mainLay->addWidget(m_lostLbl);

    setLayout(mainLay);
}

void MainWidget::start()
{
    auto cfg = GeneralOptions::global().lastConfig;

    if(cfg.samples){
        m_upperW->addWidget(m_sampleWidget = new SampleWidget(this));

        connect(m_sampleWidget, &CommonWidget::answered, this, &MainWidget::updateWidget);

        connect(m_sampleWidget, &CommonWidget::finished, this, &MainWidget::handleFinish);


    }
    // if(cfg.inequlaity){
    //     addWidget(m_inequalityWidget = new InequalityWidget(this));


    //     connect(m_inequalityWidget, &CommonWidget::answered, this, &MainWidget::updateWidget);

    //     connect(m_inequalityWidget, &CommonWidget::finished, this, &MainWidget::handleFinish);
    // }

    if(!m_upperW->count())
        return;


    m_workTimer.start();

    updateWidget();

}

void MainWidget::handleFinish()
{

    auto &conf = GeneralOptions::global().lastConfig;

    auto &configs = PersonalOptions::global().configs;


    conf.bestTime = QTime(0, 0, 0).addMSecs(m_workTimer.elapsed());


    int index = configs.indexOf(conf);

    if( index >= 0){
        configs.replace(index, GeneralOptions::global().lastConfig);
    }

    emit finished();
}


void MainWidget::updateWidget()
{
    auto randIndexWidget = GeneralOptions::randomVal(m_upperW->count(), false);

    m_upperW->setCurrentIndex(randIndexWidget);

    if(auto curW = qobject_cast<CommonWidget* >(m_upperW->currentWidget())){


        updateLost();

        curW->generate();

    }
}
