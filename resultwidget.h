#ifndef RESULTWIDGET_H
#define RESULTWIDGET_H

#include <QTabWidget>

class Result;
class QSpinBox;
class QTableWidget;
class QTimeEdit;

class CommonTab : public QWidget
{
    Q_OBJECT
public:
    CommonTab(QWidget *parent = nullptr);

    virtual void updateResults() = 0;

protected:

    QTableWidget *m_resTable;

    QSpinBox *m_rightSB;
    QSpinBox *m_wrongSB;

    QTimeEdit *m_calcTime;

    int m_countRight;
    int m_countWrong;
};

class SampleTab : public CommonTab
{
    Q_OBJECT
public:
    explicit SampleTab(QWidget *parent = nullptr);

    virtual void updateResults() override;

};

class InequalityTab : public CommonTab
{
    Q_OBJECT
public:
    explicit InequalityTab(QWidget *parent = nullptr);

    virtual void updateResults() override;
private:

};

class ResultWidget : public QTabWidget
{
    Q_OBJECT
public:
    explicit ResultWidget(QWidget *parent = nullptr);


    void updateResults();
signals:

private:

    SampleTab *m_sampleTab;
    InequalityTab *m_inequalityTab;

};

#endif // RESULTWIDGET_H
