#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QStackedWidget>
#include <QElapsedTimer>


class QLCDNumber;
class QLabel;
class DropLabel;
class SampleInfo;
class QTimer;
class QHBoxLayout;

class CommonWidget : public QWidget
{
    Q_OBJECT

public:

    CommonWidget(QWidget *parent = nullptr);

    virtual void generate();

    void configLabel(QLabel *lbl, int size = 72);
    int calc()const;

signals:

    void answered();
    void finished();

protected:

    virtual void keyPressEvent(QKeyEvent *e) override;

    virtual void checkAnswer();

    int generateValues(int &fVal, int &sVal, char sign, bool canChangeFirst = true) const;


    virtual void generateSample(SampleInfo &sample);


    int tranlateTimeToSecs(const QTime &time)const;




    QHBoxLayout *sampleLay;

    QTimer *m_limitTimer;

    QList<QLCDNumber*>m_operandsLCDs;
    QList<QLabel*>m_operatorsLbl;


    int m_countForExit;
    int len;

};

class SampleWidget : public CommonWidget
{
    Q_OBJECT
public:
    explicit SampleWidget(QWidget *parent = nullptr);

    virtual void generate() override;

protected:

    virtual void keyPressEvent(QKeyEvent *e)override;

    virtual void checkAnswer() override;
private:


    char invertSign(char sign);


    QLCDNumber *m_ansVal;
    QLCDNumber *m_enterVal;


};

class InequalityWidget : public CommonWidget
{
    Q_OBJECT
public:
    explicit InequalityWidget(QWidget *parent = nullptr);

    virtual void generate() override;

protected:

    virtual void keyPressEvent(QKeyEvent *e) override;

    virtual void checkAnswer() override;


private:

    QLCDNumber *m_leftFirstVal;
    QLCDNumber *m_leftSecondVal;

    QLCDNumber *m_rightFirstVal;
    QLCDNumber *m_rightSecondVal;

    QLabel *m_leftSignLbl;
    QLabel *m_rightSignLbl;
    DropLabel *m_compareSignLbl;

    char m_curLeftSign;
    char m_curRightSign;


};

class MainWidget : public QWidget
{
    Q_OBJECT
public:
    explicit MainWidget(QWidget *parent = nullptr);

    void start();

signals:
    void finished();

private slots:

    void updateLost();

    void handleFinish();
    void updateWidget();

private:

    QStackedWidget *m_upperW;

    SampleWidget *m_sampleWidget;
    InequalityWidget *m_inequalityWidget;

    QElapsedTimer m_workTimer;


    QLabel *m_lostLbl;
};

#endif // MAINWIDGET_H
