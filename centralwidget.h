#ifndef CENTRALWIDGET_H
#define CENTRALWIDGET_H

#include <QStackedWidget>

class MainWidget;
class ResultWidget;

class CentralWidget : public QStackedWidget
{
    Q_OBJECT
public:
    CentralWidget(QWidget *parent = nullptr);

private:

    MainWidget *m_mainWidget;
    ResultWidget *m_resultWidget;

};

#endif // CENTRALWIDGET_H
