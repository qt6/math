#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include "options.h"
#include <QDialog>

class QSpinBox;
class QCheckBox;
class QComboBox;
class QTimeEdit;

class SettingsDialog : public QDialog
{
    Q_OBJECT
public:
    SettingsDialog(QWidget *parent =nullptr);


private:

    void setData();
    void getData(const PersonalConfig &cfg);

    void checkSigns();

    QPushButton *m_okBtn;

    QSpinBox *m_countSB;
    QSpinBox *m_minValSB;
    QSpinBox *m_maxValSB;
    QSpinBox *m_operandCountSB;
    QTimeEdit *m_bestTimeTE;
    QTimeEdit *m_limitTimeTE;


    QCheckBox *m_plusChB;
    QCheckBox *m_minusChB;
    QCheckBox *m_multChB;
    QCheckBox *m_devideChB;
    QCheckBox *m_repeatChB;
    QCheckBox *m_mixChB;
    QCheckBox *m_samplesChB;
    QCheckBox *m_inequalityChB;
    QCheckBox *m_limitTimerChB;

    QComboBox *m_nameComboBox;
};



#endif // SETTINGSDIALOG_H
