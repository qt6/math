#include "settingsdialog.h"

#ifdef QT_DEBUG
#include <QDebug>
#endif

#include <QFormLayout>
#include <QLayout>
#include <QDialogButtonBox>
#include <QSpinBox>
#include <QLineEdit>
#include <QComboBox>
#include <QCheckBox>
#include <QPushButton>
#include <QTimeEdit>

#include "options.h"

SettingsDialog::SettingsDialog(QWidget *parent)
    :QDialog(parent)
{

    auto mainLay = new QFormLayout(this);

    m_nameComboBox = new QComboBox(this);
    m_countSB = new QSpinBox(this);
    m_minValSB = new QSpinBox(this);
    m_maxValSB = new QSpinBox(this);
    m_operandCountSB = new QSpinBox(this);

    m_limitTimerChB = new QCheckBox("Время на один пример", this);
    m_plusChB = new QCheckBox("+", this);
    m_minusChB = new QCheckBox("-", this);
    m_multChB = new QCheckBox("*", this);
    m_devideChB = new QCheckBox(":", this);
    m_repeatChB = new QCheckBox("Ждать правильный ответ", this);
    m_mixChB = new QCheckBox("Перемешивать", this);
    m_samplesChB = new QCheckBox("Примеры", this);
    m_inequalityChB = new QCheckBox("Неравенства", this);

    m_bestTimeTE = new QTimeEdit(this);
    m_limitTimeTE = new QTimeEdit(this);


    m_nameComboBox->setLineEdit(new QLineEdit(m_nameComboBox));


    m_bestTimeTE->setReadOnly(true);
    m_bestTimeTE->setDisplayFormat("hh:mm:ss");


#ifndef QT_DEBUG
    m_mixChB->setEnabled(false);
    m_inequalityChB->setEnabled(false);
    m_operandCountSB->setEnabled(false);
#endif



    m_limitTimeTE->setEnabled(false);
    m_limitTimeTE->setDisplayFormat("hh:mm:ss");

    m_operandCountSB->setRange(2, 5);
    m_countSB->setRange(1, 1000);
    m_minValSB->setRange(0, 1000);
    m_maxValSB->setRange(0, 1000);

    m_limitTimeTE->setButtonSymbols(QSpinBox::NoButtons);
    m_bestTimeTE->setButtonSymbols(QSpinBox::NoButtons);
    m_countSB->setButtonSymbols(QSpinBox::NoButtons);
    m_minValSB->setButtonSymbols(QSpinBox::NoButtons);
    m_maxValSB->setButtonSymbols(QSpinBox::NoButtons);
    m_operandCountSB->setButtonSymbols(QSpinBox::NoButtons);

    m_limitTimeTE->setAlignment(Qt::AlignCenter);
    m_bestTimeTE->setAlignment(Qt::AlignCenter);
    m_countSB->setAlignment(Qt::AlignCenter);
    m_minValSB->setAlignment(Qt::AlignCenter);
    m_maxValSB->setAlignment(Qt::AlignCenter);
    m_operandCountSB->setAlignment(Qt::AlignCenter);

    for(const auto &conf : PersonalOptions::global().configs)
        m_nameComboBox->addItem(conf.name, QVariant::fromValue(conf));


    auto btnBox = new QDialogButtonBox(QDialogButtonBox::Ok| QDialogButtonBox::Cancel, this);

    m_okBtn = btnBox->button(QDialogButtonBox::Ok);

    btnBox->setCenterButtons(true);

    mainLay->addRow(m_nameComboBox);
    mainLay->addRow("Прошлое время решений:", m_bestTimeTE);
    mainLay->addRow("Количество", m_countSB);
    mainLay->addRow("Минимальное значение", m_minValSB);
    mainLay->addRow("Максимальное значение", m_maxValSB);
    mainLay->addRow("Количество операндов", m_operandCountSB);
    mainLay->addRow(m_limitTimerChB, m_limitTimeTE);
    mainLay->addRow(m_repeatChB, m_mixChB);
    mainLay->addRow(m_samplesChB, m_inequalityChB);
    mainLay->addRow(m_plusChB, m_minusChB);
    mainLay->addRow(m_multChB, m_devideChB);
    mainLay->addRow(btnBox);

    m_okBtn->setEnabled(false);

    connect(btnBox, &QDialogButtonBox::accepted, this, &SettingsDialog::setData);
    connect(btnBox, &QDialogButtonBox::accepted, this, &SettingsDialog::accept);

    connect(btnBox, &QDialogButtonBox::rejected, this, &QDialog::reject);

    connect(m_plusChB, &QCheckBox::toggled, this, &SettingsDialog::checkSigns);
    connect(m_minusChB, &QCheckBox::toggled, this, &SettingsDialog::checkSigns);
    connect(m_multChB, &QCheckBox::toggled, this, &SettingsDialog::checkSigns);
    connect(m_devideChB, &QCheckBox::toggled, this, &SettingsDialog::checkSigns);

    connect(m_limitTimerChB, &QCheckBox::toggled, m_limitTimeTE, &QLineEdit::setEnabled);

    connect(m_samplesChB, &QCheckBox::toggled, this, &SettingsDialog::checkSigns);
    connect(m_inequalityChB, &QCheckBox::toggled, this, &SettingsDialog::checkSigns);

    connect(m_nameComboBox, qOverload<int>(&QComboBox::currentIndexChanged), [this]{
        getData(m_nameComboBox->currentData().value<PersonalConfig>());
    });

    getData(GeneralOptions::global().lastConfig);

}

void SettingsDialog::setData()
{
    auto &lastConf = GeneralOptions::global().lastConfig;
    auto &pOpt = PersonalOptions::global();

    lastConf.name = m_nameComboBox->currentText();
    lastConf.minVal = m_minValSB->value();
    lastConf.maxVal = m_maxValSB->value();
    lastConf.count = m_countSB->value();
    lastConf.countOperands = m_operandCountSB->value();
    lastConf.lost = lastConf.count;
    lastConf.repeat = m_repeatChB->isChecked();
    lastConf.mix = m_mixChB->isChecked();
    lastConf.inequlaity = m_inequalityChB->isChecked();
    lastConf.samples = m_samplesChB->isChecked();

    lastConf.isPlus = m_plusChB->isChecked();
    lastConf.isMinus = m_minusChB->isChecked();
    lastConf.isMult = m_multChB->isChecked();
    lastConf.isDev = m_devideChB->isChecked();

    lastConf.isLimited = m_limitTimerChB->isChecked();

    lastConf.bestTime = m_bestTimeTE->time();
    lastConf.limitTime = m_limitTimeTE->time();

    if(m_plusChB->isChecked())
        lastConf.signs.append('+');
    if(m_minusChB->isChecked())
        lastConf.signs.append('-');
    if(m_multChB->isChecked())
        lastConf.signs.append('*');
    if(m_devideChB->isChecked())
        lastConf.signs.append(':');

    int fIndex = pOpt.configs.indexOf(lastConf);


    if(fIndex >= 0){

        pOpt.configs.replace(fIndex, lastConf);

    }else{
        pOpt.configs.append(lastConf);
    }

}

void SettingsDialog::getData(const PersonalConfig &cfg)
{

    m_operandCountSB->setValue(cfg.countOperands);
    m_countSB->setValue(cfg.count);
    m_minValSB->setValue(cfg.minVal);
    m_maxValSB->setValue(cfg.maxVal);
    m_repeatChB->setChecked(cfg.repeat);
    m_mixChB->setChecked(cfg.mix);
    m_inequalityChB->setChecked(cfg.inequlaity);
    m_samplesChB->setChecked(cfg.samples);

    m_limitTimerChB->setChecked(cfg.isLimited);
    m_plusChB->setChecked(cfg.isPlus);
    m_minusChB->setChecked(cfg.isMinus);
    m_multChB->setChecked(cfg.isMult);
    m_devideChB->setChecked(cfg.isDev);
    m_bestTimeTE->setTime(cfg.bestTime);
    m_limitTimeTE->setTime(cfg.limitTime);

    m_nameComboBox->setCurrentText(cfg.name);

}

void SettingsDialog::checkSigns()
{
    m_okBtn->setEnabled((m_plusChB->isChecked() ||
                         m_minusChB->isChecked() ||
                         m_multChB->isChecked() ||
                         m_devideChB->isChecked() ) && (
                            m_samplesChB->isChecked() ||
                            m_inequalityChB->isChecked() )
                        );
}
