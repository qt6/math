#include "centralwidget.h"

#include <QApplication>

#include "options.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    OptionSaver saver;
    CentralWidget w;
    w.showMaximized();
    return a.exec();
}
