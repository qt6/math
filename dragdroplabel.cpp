#include "dragdroplabel.h"

#include <QMouseEvent>

#include <QDrag>
#include <QMimeData>

DragLabel::DragLabel(const QString &text, QWidget *parent)
    : QLabel(text, parent)
{
    setAlignment(Qt::AlignCenter);
}

void DragLabel::mousePressEvent(QMouseEvent *e)
{

    if (e->button() == Qt::LeftButton
         && geometry().contains(mapToGlobal(e->pos()))) {

         QDrag *drag = new QDrag(this);
         QMimeData *mimeData = new QMimeData;

         mimeData->setText(text());
         drag->setMimeData(mimeData);

         Qt::DropAction dropAction = drag->exec();
     }
}

DropLabel::DropLabel(const QString &text, QWidget *parent)
    : QLabel(text, parent)
{
    setAlignment(Qt::AlignCenter);
    setAcceptDrops(true);
}

void DropLabel::dragEnterEvent(QDragEnterEvent *e)
{
    if (e->mimeData()->hasFormat("text/plain"))
            e->acceptProposedAction();
}

void DropLabel::dropEvent(QDropEvent *e)
{
    setText(e->mimeData()->text());
    e->acceptProposedAction();
}
